

deal.II
=======

This page contains information about `deal.II`__

__ http://www.dealii.org


Installing deal.II on your department linux account (Scientific linux 6)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

To install deal.II (release 8.2.1) on your department you will have first have  to install `CMake`__ as the default version is pre-historic. Installing CMake should be painless, just download the latest version and follow the instructions in the README.rst file. The installation should amount to something like:

.. code-block:: none
   
   % cd cmake-3.1.0/
   % ./bootstrap --prefix=/homes/appelo/cmake
   % make -j 8
   % make install


If the install of CMake succeeds you should add it the path by changing your ``.bashrc`` or ``.cshrc`` depending on what shell you use. 

Next download deal and follow the instructions in the README.md file in the unpacked deal source directory **note that I had to disable hdf5**

.. code-block:: none

   % cmake cmake -DDEAL_II_WITH_HDF5=OFF \
     -DCMAKE_INSTALL_PREFIX=/homes/appelo/deal .. \
     -DDEAL_II_LINKER_FLAGS_DEBUG=-lrt -DDEAL_II_LINKER_FLAGS=-lrt

   % make -j8 install

This may take some time and it may be a good idea to start reading the description of `step 1`__ 

Once deal has compiled and installed you can go to the examples
directory and try out if the example in step-1 works.


__ http://www.cmake.org
__ https://www.dealii.org/developer/doxygen/deal.II/step_1.html
