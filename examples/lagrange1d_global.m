function void = lagrange1d_global 

H = [];
E = [];
for N = 2;
    k = 5;
    z = linspace(0,1,N+2)';
    x = -5 + 10*z;
    x(1) = -5;
    x(N+2) = 5;
    t = linspace(-5,5,1005)';
    
    % Nodes
    xi = zeros(N+1,k+1);
    
    for i = 0:N
        hi = x(1+i+1)-x(1+i);
        xi(1+i,:) = x(1+i) + (0:k)/k*hi;
    end
    f = zeros(size(t));
    for i = 0:N
        for m = 0:k-1
            f = f + sin(pi*xi(1+i,1+m))*phi_j(t,xi,x,i,m,k,N);
        end
    end
    i = N+1;
    m = 0;
    f = f + sin(pi*xi(N+1,k+1))*phi_j(t,xi,x,i,m,k,N); 
    plot(t,f,t,sin(pi*t),'*')
    drawnow 
    H = [H; max(abs(diff(x)))];
    E = [E; max(abs(f-sin(pi*t)))];  
end

%loglog(H,E,'*',H,H.^((k+1)))

function phi = phi_j(t,xi,x,i,m,k,N)

if (m == 0)
    if(i == 0)
        phi2 = lag(t,xi(1+i,:),0);
        phi = phi2.*(t > x(1+i)).*(t < x(1+i+1));
    elseif(i == N+1)
        phi1 = lag(t,xi(1+i-1,:),k);
        phi = phi1.*(t > x(1+i-1)).*(t < x(1+i));
    else
        phi1 = lag(t,xi(1+i-1,:),k);
        phi2 = lag(t,xi(1+i,:),0);
        phi = phi1.*(t > x(1+i-1)).*(t < x(1+i)) + phi2.*(t > x(1+i)).*(t < x(1+i+1));
    end
else
    if(i == 0)
        phi = lag(t,xi(1+i,:),m);
        phi = phi.*(t > x(1+i)).*(t < x(1+i+1));
    elseif(i == N+1)
        phi = lag(t,xi(1+i-1,:),m);
        phi = phi.*(t > x(1+i-1)).*(t < x(1+i+1-1));
    else
        phi = lag(t,xi(1+i,:),m);
        phi = phi.*(t > x(1+i)).*(t < x(1+i+1));
    end
end

function p = lag(t,s,m)

sl = s;
sl(1+m) = [];
p = ones(size(t));
for i = 1:length(sl)
    p = p.*((t-sl(i))/(s(1+m)-sl(i)));
end




