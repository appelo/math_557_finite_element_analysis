.. MATH-557-FEM documentation master file, created by
   sphinx-quickstart on Tue Dec 30 09:27:22 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


News
----

General Information
-------------------

.. toctree::
   :maxdepth: 1

   Syllabus
   Schedule

Technical Information
---------------------

.. toctree::
   :maxdepth: 1

   GIT
   dealii
   CPP
   
Homework
--------

.. toctree::
   :maxdepth: 1

   HW1
   HW2
   HW3


