
Syllabus
========

Description:
------------

This is course in Finite Element Analysis.

Instructor:
-----------

Daniel Appelö. appelo @ math.unm.edu

Location:
---------
TR 0930-1045 SMLC 124

Material:
---------

There are three books for this class:

 * *Theory and Practice of Finite Elements* by Alexandre Ern and Jean-Luc Guermond.
 * *Numerical Solution of Partial Differential Equations by the Finite
   Element Method* by Claes Johnson.
 * *Nodal discontinuous Galerkin methods : algorithms, analysis, and
   applications* by Jan S Hesthaven and Tim Warburton.

We will also use the tutorials in `deal.II`__ as well as the `video
lectures by Wolfgang Bangerth.`__   

__ http://www.dealii.org
__ http://www.math.tamu.edu/~bangerth/videos.html


Office hours:
-------------

TR: 08.00-09.30. 
Room: SMLC 310.


Grading:
--------

The grading will be based on the homework assignments. 


Homework / Computer Projects:
-----------------------------

The homework will consist of theoretical and computational assignments
that you will hand in. You are *strongly* encouraged to work in pairs
for the homework but it is expected that both of you can explain the
material. For the write-ups of the assignments we will use a version
control system explained in the first homework. 

Dishonesty policy:
------------------

Each student is expected to maintain the highest standards of honesty and integrity in academic and professional matters. The University reserves the right to take disciplinary action, including dismissal, against any student who is found responsible for academic dishonesty. Any student who has been judged to have engaged in academic dishonesty in course work may receive a reduced or failing grade for the work in question and/or for the course.
Academic dishonesty includes, but is not limited to, dishonesty on quizzes, tests or assignments; claiming credit for work not done or done by others; and hindering the academic work of other students.

American disabilities act:
--------------------------

In accordance with University Policy 2310 and the American Disabilities Act (ADA), academic accommodations may be made for any student who notifies the instructor of the need for an accommodation. It is imperative that you take the initiative to bring such needs to the instructor's attention, as the instructor is not legally permitted to inquire. Students who may require assistance in emergency evacuations should contact the instructor as to the most appropriate procedures to follow. Contact Accessibility Services at 505-661-4692 for additional information

Disclaimer:
-----------

I reserve the right to make reasonable and necessary changes to the policies outlined in this syllabus. Whenever possible, the class will be notified well in advance of such changes. An up-to-date copy of the syllabus can always be found on the course website. It is your responsibility to know and understand the policies discussed therein. If in doubt, ask questions.

