
[x1,x2] = meshgrid(linspace(0,1,500));

lam0 = 1 - x1 - x2;
lam1 = x1;
lam2 = x2;

tri = ((x2+x1)<1);

figure(1)
subplot(1,3,1)
contourf(x1,x2,lam0./tri,30,'linestyle','none')
axis equal

subplot(1,3,2)
contourf(x1,x2,lam1./tri,30,'linestyle','none')
axis equal

subplot(1,3,3)
contourf(x1,x2,lam2./tri,30,'linestyle','none')
axis equal

theta0 = lam0.*(2*lam0-1);
theta1 = lam1.*(2*lam1-1);
theta2 = lam2.*(2*lam2-1);
theta3 = 4*lam0.*lam1;
theta4 = 4*lam0.*lam2;
theta5 = 4*lam1.*lam2;

figure(2)
subplot(2,3,1)
mesh(x1,x2,theta0./tri)
axis equal

subplot(2,3,2)
mesh(x1,x2,theta1./tri)
axis equal

subplot(2,3,3)
mesh(x1,x2,theta2./tri)
axis equal

subplot(2,3,4)
mesh(x1,x2,theta3./tri)
axis equal

subplot(2,3,5)
mesh(x1,x2,theta4./tri)
axis equal

subplot(2,3,6)
mesh(x1,x2,theta5./tri)
axis equal

%
k = 3;
theta0 = 0.5*(3*lam0-1).*(3*lam0-2);
theta1 = 0.5*(3*lam1-1).*(3*lam1-2);
theta2 = 0.5*(3*lam2-1).*(3*lam2-2);
theta3 = 9/2*lam0.*(3*lam0-1).*lam1;
theta4 = 9/2*lam0.*(3*lam0-1).*lam2;
theta5 = 9/2*lam1.*(3*lam1-1).*lam0;
theta6 = 9/2*lam1.*(3*lam1-1).*lam2;
theta7 = 9/2*lam2.*(3*lam2-1).*lam0;
theta8 = 9/2*lam2.*(3*lam2-1).*lam1;
theta9 = 27*lam0.*lam1.*lam2;


figure(3)
subplot(3,3,1)
mesh(x1,x2,theta0./tri)
axis equal

subplot(3,3,2)
mesh(x1,x2,theta1./tri)
axis equal

subplot(3,3,3)
mesh(x1,x2,theta2./tri)
axis equal

subplot(3,3,4)
mesh(x1,x2,theta3./tri)
axis equal

subplot(3,3,5)
mesh(x1,x2,theta4./tri)
axis equal

subplot(3,3,6)
mesh(x1,x2,theta5./tri)
axis equal

subplot(3,3,7)
mesh(x1,x2,theta6./tri)
axis equal

subplot(3,3,8)
mesh(x1,x2,theta7./tri)
axis equal

subplot(3,3,9)
mesh(x1,x2,theta8./tri)
axis equal

