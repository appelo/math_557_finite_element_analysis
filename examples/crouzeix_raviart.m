[x1,x2] = meshgrid(linspace(0,1,500));

d = 2;
lam0 = d*(1/d-lam0);
lam1 = d*(1/d-lam1);
lam2 = d*(1/d-lam2);

tri = ((x2+x1)<1);

figure(1)
subplot(1,3,1)
mesh(x1,x2,lam0./tri)
axis equal

subplot(1,3,2)
mesh(x1,x2,lam1./tri)
axis equal

subplot(1,3,3)
mesh(x1,x2,lam2./tri)
axis equal
