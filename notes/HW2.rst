
Homework 2, due 08.00, 24/2-2015
================================

Individual tasks
----------------
For these hand in your own report. Discussion within groups are
encouraged.  


  1. EG 1.1
  2. EG 1.3
  3. (By Tom Hagstrom) Write a program to compute quadratic interpolants on triangles using
     the nodal configuration described in Proposition 1.34 and shown in Table 1.1.
     Use it to interpolate the function :math:`f(x,y)=\sin{(8 \pi x y)}` on a
     mesh defined by:

     1. Breaking the unit square into squares of size :math:`h_1=1/25`, 
        :math:`h_2 = 1/50`, and :math:`h_3 = 1/100`. The vertices of
        the square :math:`(j,k)` will be :math:`(jh,kh)`,
        :math:`((j+1)h,kh),\, (jh,(k+1)h),\,((j+1)h,(k+1)h)`.
     2. Breaking each square into two triangles with vertices  
        :math:`(jh,kh), \, ((j+1)h,kh), \, ((j+1)h,(k+1)h)` and :math:`(jh,kh),\, 
        (jh,(k+1)h), \, ((j+1)h,(k+1)h)`.
	

     In each case compute the error in interpolating the function and its first
     derivatives at the point in each triangle whose coordinates are the average
     of the coordinates of the vertices :math:`((j+2/3)h,(k+1/3)h)` and 
     :math:`((j+1/3)h,(k+2/3)h)` respectively. Comment on the convergence rate of the maximum
     errors - does it conform to theory? 


Group tasks
-----------
Write one report per group. Check in the source code for the modified
examples. 
 
     1. Modify the Step-1 example program, using appropriate functions
	from the ``GridGenerator`` class, to build a cylinder in 3
	dimensions with radius 1 and height 4.

     2. Refine cells once in the upper half if they lie within 1/2 of the center,
	and twice if they lie closer to the outer boundary.

     3. Refine cells twice in the lower half if they lie within 2/3 of
	the center and three times if they lie closer to the outer
	boundary.

     4.	Plot the resulting grid. 

     5. Modify the function from the previous problem and the code in Step-2
	to display the sparsity patterns for continuous elements of degrees 1 and
	4 after applying the Cuthill-McKee reordering. Plot the sparsity pattern.
	How does it differ from the results in the tutorial?
	How does it differ as the degree changes. 

     6. Repeat the previous problem using the lowest order Nedelec and
	Raviart-Thomas elements. You can find a description of these in the finite
	element space descriptions documentation.
