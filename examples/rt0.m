clear
[x1,x2] = meshgrid(linspace(0,1,10));

measK = 1/2;
a0 = [0,0]';
a1 = [1,0]';
a2 = [0,1]';

d = 2;
theta01 = (x1-a0(1))/(d*measK);
theta02 = (x2-a0(2))/(d*measK);

theta11 = (x1-a1(1))/(d*measK);
theta12 = (x2-a1(2))/(d*measK);

theta21 = (x1-a2(1))/(d*measK);
theta22 = (x2-a2(2))/(d*measK);

tri = ((x2+x1)<1);
figure(1)
subplot(1,3,1)
quiver(x1,x2,(theta01./tri).*tri,(theta02./tri).*tri)
axis equal

subplot(1,3,2)
quiver(x1,x2,(theta11./tri).*tri,(theta12./tri).*tri)
axis equal

subplot(1,3,3)
quiver(x1,x2,(theta21./tri).*tri,(theta22./tri).*tri)
axis equal
