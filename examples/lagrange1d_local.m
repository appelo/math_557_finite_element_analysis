function void = lagrange1d_local

% Choose the order
k = 5;
% Evaluate the Lagrange basis on the reference element [0,1]
t = linspace(0,1,400)';
s = (0:k)/k;
P = zeros(length(t),length(s));
for m = 0:k
    P(:,m+1) = lag(t,s,m);
end
H = [];
E = [];
for N = 2:2:100;
    % Set up a grid.
    z = linspace(0,1,N+2)';
    x = -5 + 10*z;
    x(1) = -5;
    x(N+2) = 5;
    
    fg = [];
    xg = [];
    for i = 0:N
        hi = x(1+i+1)-x(1+i);
        xi = x(1+i) + (0:k)'/k*hi;
        xloc = x(1+i) + t*hi;
        floc = P*sin(pi*xi);
        xg = [xg; xloc];
        fg = [fg; floc];
    end
    plot(xg,fg,xg,sin(pi*xg),'*')
    drawnow 
    H = [H; max(abs(diff(x)))];
    E = [E; max(abs(fg-sin(pi*xg)))];  
end

loglog(H,E,'*',H,H.^((k+1)))

function p = lag(t,s,m)

sl = s;
sl(1+m) = [];
p = ones(size(t));
for i = 1:length(sl)
    p = p.*((t-sl(i))/(s(1+m)-sl(i)));
end




