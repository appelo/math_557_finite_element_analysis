.. -*- rst -*- -*- restructuredtext -*-

.. _SCHEDULE:

================================
Schedule (preliminary)
================================

 * Week 1 Version control, GIT & introduction to finite elements EG 1.1-1.2
 * Week 2 Meshes EG 1.2-1.3
 * Week 3 Approximation and interpolation EG 1.4-1.5
 * Week 4 Data structures EG 1.5-7.1
 * Week 5 Mesh generation, introduction to deal.II EG 7.2
 * Week 6 Meshes and data structures in deal.II, BNB Theorem EG 2.1
 * Week 7 Galerkin methods EG 2.1-2.3
 * Week 8 Error analysis and saddle point problems EG 2.3-2.4
 * Week 9 spring break
 * Week 10 Application to scalar elliptic pdes EG 2.4, 3.1-3.2
 * Week 11 Applications in continuum mechanics EG 3.2,3.4
 * Week 12 Solving elliptic problems using deal.II 
 * Week 13 Mixed problems EG 4.1-4.2.
 * Week 14 Solving mixed problems using deal.II, first order equations EG 4.2,5.1
 * Week 15 :math:`L^2`-theory, introduction to discontinuous Galerkin methods EG 5.1-5.2, 5.6
 * Week 16 Discontinuous Galerkin methods in deal.II EG 5.6

Most likely this will change.
   

